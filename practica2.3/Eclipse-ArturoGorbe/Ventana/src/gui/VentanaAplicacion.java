package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.ImageIcon;
/**
 * 
 * @author Arturo Gorbe
 * @since 14/12/2017
 */
public class VentanaAplicacion extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaAplicacion frame = new VentanaAplicacion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaAplicacion() {
		setTitle("Crea tu propio mando");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaAplicacion.class.getResource("/imagen/xbox-icon.png")));
		setBackground(Color.GRAY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmGuardarModelo = new JMenuItem("Guardar modelo");
		mnArchivo.add(mntmGuardarModelo);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnArchivo.add(mntmSalir);
		
		JMenu mnConfiguracion = new JMenu("Ayuda");
		menuBar.add(mnConfiguracion);
		
		JMenu mnConfiguracion_1 = new JMenu("Configuracion");
		mnConfiguracion.add(mnConfiguracion_1);
		
		JMenuItem mntmAutomatica = new JMenuItem("Automatica");
		mnConfiguracion_1.add(mntmAutomatica);
		
		JMenuItem mntmManual = new JMenuItem("Manual");
		mnConfiguracion_1.add(mntmManual);
		
		JMenuItem mntmLicenciaDeContrato = new JMenuItem("Licencia de contrato");
		mnConfiguracion.add(mntmLicenciaDeContrato);
		
		JMenuItem mntmSaberMas = new JMenuItem("Saber mas");
		mnConfiguracion.add(mntmSaberMas);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPonTuNombre = new JLabel("Pon tu nombre en el mando");
		lblPonTuNombre.setBounds(10, 33, 142, 14);
		contentPane.add(lblPonTuNombre);
		
		textField = new JTextField();
		textField.setBounds(162, 30, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Simple", "A rayas", "Dos colores"}));
		comboBox.setBounds(299, 30, 104, 20);
		contentPane.add(comboBox);
		
		JLabel lblEligeElPatron = new JLabel("Elige el patron de dise\u00F1o");
		lblEligeElPatron.setBounds(299, 11, 136, 14);
		contentPane.add(lblEligeElPatron);
		
		JSlider slider = new JSlider();
		slider.setBounds(224, 156, 200, 26);
		contentPane.add(slider);
		
		JLabel lblBrilloDeLos = new JLabel("Brillo de los colores");
		lblBrilloDeLos.setBounds(284, 131, 119, 14);
		contentPane.add(lblBrilloDeLos);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(353, 100, 29, 20);
		contentPane.add(spinner);
		
		JLabel lblNumeroDeMandos = new JLabel("Numero de mandos");
		lblNumeroDeMandos.setBounds(315, 75, 119, 14);
		contentPane.add(lblNumeroDeMandos);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Deseo recibir notificaciones");
		chckbxNewCheckBox.setBackground(Color.GRAY);
		chckbxNewCheckBox.setBounds(10, 151, 160, 23);
		contentPane.add(chckbxNewCheckBox);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Poner logotipo XBOX");
		rdbtnNewRadioButton.setBackground(Color.GRAY);
		rdbtnNewRadioButton.setBounds(10, 71, 160, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Carcasa extra");
		rdbtnNewRadioButton_1.setBackground(Color.GRAY);
		rdbtnNewRadioButton_1.setBounds(10, 99, 160, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Protector de Joysticks");
		rdbtnNewRadioButton_2.setBackground(Color.GRAY);
		rdbtnNewRadioButton_2.setBounds(10, 125, 160, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setForeground(Color.GREEN);
		btnEnviar.setBounds(102, 206, 89, 23);
		contentPane.add(btnEnviar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setForeground(Color.RED);
		btnCancelar.setBounds(227, 206, 89, 23);
		contentPane.add(btnCancelar);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(VentanaAplicacion.class.getResource("/imagen/xbox-icon.png")));
		lblNewLabel.setBounds(176, 75, 72, 70);
		contentPane.add(lblNewLabel);
	}
}
