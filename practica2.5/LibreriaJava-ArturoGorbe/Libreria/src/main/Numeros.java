package main;
import java.util.Scanner;
public class Numeros {
	int numero;
	static Scanner in=new Scanner(System.in);
	
	void pedirnumero() {
	
		do{
		System.out.println("Dame un numero:");
		this.numero=in.nextInt();
		if(numero<=0){
			System.out.println("Error, el numero no puede ser menor que 1");
		}
		}
		while(numero<=0);
		System.out.println("Has elegido el numero: "+numero);
		
	}
	
	void primo(){
		int contador=0;
		for(int i=1; i<=numero; i++){
			if(numero%i==0){
				contador++;
			}
		}
		if(contador==2){
			System.out.println("El numero "+numero+ " es primo");
		}
		 else{
			System.out.println("El numero "+numero+ " no es primo");
		}
	}
	
	void factorizar(){
		int factorizar=1;
		for(int i=1; i<=numero; i++){
			factorizar=factorizar*i;
		}
		System.out.println("La factorizacion de "+numero+" es "+factorizar);
	}
	
	void potencia(){
		int exponente;
		double potencia;
		System.out.println("�cuanto quieres elevar la potencia de " +numero+ "?");
		exponente=in.nextInt();
		potencia=Math.pow(numero, exponente);
		System.out.println(numero+" elevado a "+exponente+ " es igual a "+potencia);
	}
	
	void multiplicar(){
		System.out.println("La tabla del "+numero);
		for(int i=1; i<=10; i++){
			int multiplicar=numero*i;
			System.out.println(numero+" * "+i+" = "+multiplicar);
		}
	}
}
