﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVS_ArturoGorbe;

namespace LibreriaVS2_ArturoGorbe
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion = 0;
            Class1 menu1 = new Class1();
            Class2 numero1 = new Class2();

            do
            {

                Console.WriteLine("Elige una opcion");
                Console.WriteLine("1-Menu de Xbox");
                Console.WriteLine("2-Numeros");
                opcion = int.Parse(Console.ReadLine());
                if (opcion < 1 || opcion > 2)
                {
                    Console.WriteLine("Error");
                }
                if (opcion == 1)
                {
                    menu1.menu();
                }
                else if (opcion == 2)
                {
                    numero1.pedirnumero();
                    numero1.primo();
                    numero1.factorizar();
                    numero1.potencia();
                    numero1.multiplicar();
                    Console.ReadKey();
                }
            }
            while (opcion < 1 || opcion > 2);

        }
    }
}
