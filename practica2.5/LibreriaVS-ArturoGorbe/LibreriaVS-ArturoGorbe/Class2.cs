﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibreriaVS_ArturoGorbe
{

    public class Class2
{
        int numero;
        

       public void pedirnumero()
        {

            do
            {
                Console.WriteLine("Dame un numero:");
                this.numero = int.Parse(Console.ReadLine());
                if (numero <= 0)
                {
                    Console.WriteLine("Error, el numero no puede ser menor que 1");
                }
            }
            while (numero <= 0);
            Console.WriteLine("Has elegido el numero: " + numero);

        }

        public void primo()
        {
            int contador = 0;
            for (int i = 1; i <= numero; i++)
            {
                if (numero % i == 0)
                {
                    contador++;
                }
            }
            if (contador == 2)
            {
                Console.WriteLine("El numero " + numero + " es primo");
            }
            else
            {
                Console.WriteLine("El numero " + numero + " no es primo");
            }
        }

        public void factorizar()
        {
            int factorizar = 1;
            for (int i = 1; i <= numero; i++)
            {
                factorizar = factorizar * i;
            }
            Console.WriteLine("La factorizacion de " + numero + " es " + factorizar);
        }

        public void potencia()
        {
            int exponente;
            double potencia;
            Console.WriteLine("¿cuanto quieres elevar la potencia de " + numero + "?");
            exponente = int.Parse(Console.ReadLine());
            potencia = Math.Pow(numero, exponente);
            Console.WriteLine(numero + " elevado a " + exponente + " es igual a " + potencia);
        }

       public void multiplicar()
        {
            Console.WriteLine("La tabla del " + numero);
            for (int i = 1; i <= 10; i++)
            {
                int multiplicar = numero * i;
                Console.WriteLine(numero + " * " + i + " = " + multiplicar);
            }
        }
    }
}
