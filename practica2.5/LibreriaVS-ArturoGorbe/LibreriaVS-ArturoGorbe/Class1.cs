﻿using System;

namespace LibreriaVS_ArturoGorbe
{
    public class Class1
    {

        public void menu()
        {
           
            int menu;
            String premium2 = "";
            String nick = "";
            do
            {

                Console.WriteLine("Bienvenido al menu de Xbox");
                Console.WriteLine("1-Mostrar datos de usuario");
                Console.WriteLine("2-Crear usuario");
                Console.WriteLine("3-¿Deseas pagar el servicio premium?");
                Console.WriteLine("4-Salir");
                Console.Write("");

                Console.WriteLine("Elige una opcion:");
            
                menu = int.Parse(Console.ReadLine());
                switch (menu)
                {
                    case 1: mostrar(premium2, nick); break;
                    case 2: nick = crear(); break;
                    case 3: premium2 = premium(); break;
                    case 4: cerrar(); break;
                    default: Console.WriteLine("Error"); break;
                }
            }
            while (menu != 4);
        }



        public static void cerrar()
        {
           
            Console.WriteLine("Hasta la proxima");
            Console.ReadKey();
        }

        public static String premium()
        {
           
            int premium;
            String premium2 = "";
            Console.WriteLine("¿Deseas pagar el servicio premium?");
            Console.WriteLine("1-Si");
            Console.WriteLine("2-No");
            premium = int.Parse(Console.ReadLine());
            if (premium == 1)
            {
                premium2 = "Activado";
            }
            else
            {
                premium2 = "Desactivado";
            }
            return premium2;
        }

        public static String crear()
        {
          
            String nick;
            Console.WriteLine("Nick del jugador");
            nick = Console.ReadLine();
            return nick;
        }

        public static void mostrar(String premium2, String nick)
        {
            
            Console.WriteLine("Nick de usuario: " + nick);
            Console.WriteLine("Servicio premium: " + premium2);
        }
    }
}
